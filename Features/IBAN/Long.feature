Feature: Verify that long IBAN code.

  Scenario: Submit long IBAN code
    Given post request for "bank-account" method is set up
    When post request with "long" IBAN code is submitted
    Then response verifies "long" length of IBAN code
