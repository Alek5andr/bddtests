Feature: Verify that "riskCheckMessage.code" is "200.909" for unsupported country IBAN code.

  Scenario: Submit unsupported country IBAN code
    Given post request for "bank-account" method is set up
    When post request with "unsupported" IBAN code is submitted
    Then response's 'riskCheckMessage.code' is "200.909"
