Feature: Verify that response status code is 200 for valid IBAN code.

  Scenario: Submit valid IBAN code
    Given post request for "bank-account" method is set up
    When post request with "valid" IBAN code is submitted
    Then response's status code 200 is verified
