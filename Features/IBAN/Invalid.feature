Feature: Verify that "riskCheckMessage.code" is "200.908" for invalid IBAN code.

  Scenario: Submit invalid IBAN code
    Given post request for "bank-account" method is set up
    When post request with "invalid" IBAN code is submitted
    Then response's 'riskCheckMessage.code' is "200.908"
