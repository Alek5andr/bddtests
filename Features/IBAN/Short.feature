Feature: Verify that short IBAN code.

  Scenario: Submit short IBAN code
    Given post request for "bank-account" method is set up
    When post request with "short" IBAN code is submitted
    Then response verifies "short" length of IBAN code
