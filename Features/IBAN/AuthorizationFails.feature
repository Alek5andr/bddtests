Feature: Verify that authorization fails for valid IBAN code due to invalid authorization header's value.

  Scenario: Submit valid IBAN code with invalid authorization header
    Given post request for "bank-account" method is set up
    And request's header "X-Auth-Key" with value "invalid_uthorization" is set up
    When post request with "valid" IBAN code is submitted
    Then response's status code 401 is verified
