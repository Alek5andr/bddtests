## Assignment in BDD style.
https://docs.google.com/document/d/1f8h-11d9sM9VO8OXwhk09BATBmxFklYUvMZOpig0xAs/edit#heading=h.wxf0npc71n5s

### Run tests via command prompt
- Clone project as Maven project.
- Navigate to projects directory.
- Execute the command: "mvn test -Dtest=RunAllApiTests".


Cucumber report of run features is generated into directory '_.\target\cukes_' and can be accessed with opening '_index.html_' file.

Extended log is written to directory '_.\Log4j_' and can be accessed by opening '_log4j-application.log_' file.

### SQL
```
SELECT DeviceHash
FROM pts_transactions 
WHERE FROM_UNIXTIME(`Unixtime`) > DATE_SUB(NOW(), INTERVAL 7 DAY)
GROUP BY FROM_UNIXTIME(`Unixtime`)
HAVING COUNT(DISTINCT LastName) > 2;
```

