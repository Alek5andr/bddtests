package stepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import stepDefinitions.api.RestUtil;
import utilities.Log;

public class Hooks {

  @Before
  public void beforeScenario() {
    Log.info("========== Start test ==========");
    RestUtil.setBaseURI("https://dev.horizonafs.com/ecommercerest/");
  }

  @After
  public void afterScenario() {
    RestUtil.resetBaseURI();
    RestUtil.resetBasePath();
    Log.info("========== End test ==========\n");
  }

}
