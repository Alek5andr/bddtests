package stepDefinitions.api.post;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import enums.IBANS;
import payload.IBAN;
import stepDefinitions.api.RestUtil;
import utilities.Log;

public class BankAccount {
  private final String AUTHORIZATION_HEADER_NAME = "X-Auth-Key";
  private static IBAN iban = new IBAN();
  private static Response res = null;
  private static String ibanCode = null;

  @Given("^post request for \"([^\"]*)\" method is set up$")
  public void requestForMethodIsSetup(String method) {
    Log.info("Setting up '" + method + "' method for post request.");
    switch(method) {
      case "bank-account":
        RestUtil.setBasePath("api/v3/validate/bank-account");
        RestUtil.setContentTypeWithCharset(ContentType.JSON, "UTF-8");
        RestUtil.setHeader("Accept", ContentType.JSON.getAcceptHeader());
        RestUtil.setHeader(AUTHORIZATION_HEADER_NAME, "Q7DaxRnFls6IpwSW1SQ2FaTFOf7UdReAFNoKY68L");
        break;
      default:
        throw new IllegalArgumentException("No such method is implemented yet - " + method);
    }
  }

  @When("^post request with \"([^\"]*)\" body is submitted$")
  public static void getPostResponse(String jsonBody) {
    Log.info("Submitting request with body: " + jsonBody);
    res = RestUtil.getRequestSpecification().headers(RestUtil.getHeadersMap()).body(jsonBody).post();

    Log.info("Received response: " + res.body().asString());
  }

  @When("^post request with \"([^\"]*)\" IBAN code is submitted$")
  public static void getPostResponseForSpecificIBAN(String ibanType) {
    Log.info("Submitting post request with '" + ibanType + "' IBAN code.");
    String jsonBody = null;

    switch(ibanType) {
      case "valid":
        jsonBody = iban.getJsonBody(IBANS.validIBAN.getIBAN());
        break;
      case "invalid":
        jsonBody = iban.getJsonBody(IBANS.invalidIBAN.getIBAN());
        break;
      case "unsupported":
        jsonBody = iban.getJsonBody(IBANS.unsupportedCountryIBAN.getIBAN());
        break;
      case "short":
        ibanCode = IBANS.shortIBAN.getIBAN();
        jsonBody = iban.getJsonBody(ibanCode);
        break;
      case "long":
        ibanCode = IBANS.longIBAN.getIBAN();
        jsonBody = iban.getJsonBody(ibanCode);
        break;
      default:
        throw new IllegalArgumentException("No such IBAN type is implemented - " + ibanType);
    }
    getPostResponse(jsonBody);
  }

  public static Response getRes() {
    return res;
  }

  public static String getIBANCode() {
    return ibanCode;
  }
}
