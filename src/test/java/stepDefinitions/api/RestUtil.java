package stepDefinitions.api;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;
import cucumber.api.java.en.Given;
import utilities.Log;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

public class RestUtil {
  private static Map<String, String> headersMap = new HashMap<String, String>();
  private static RequestSpecification requestSpecification = null;

  public static void setBaseURI (String baseURI){
    Log.info("Setting RestAssured.baseURI = " + baseURI);
    RestAssured.baseURI = baseURI;
  }

  @Given("^base path \"([^\"]*)\" is set up$")
  public static void setBasePath(String basePath){
    Log.info("Setting up RestAssured.basePath = " + basePath);
    RestAssured.basePath = basePath;
  }

  public static void resetBaseURI (){
    Log.info(getMessageAboutWhatRestAssuredMethodIsReset("baseURI"));
    RestAssured.baseURI = null;
  }

  public static void resetBasePath() {
    Log.info(getMessageAboutWhatRestAssuredMethodIsReset("basePath"));
    RestAssured.basePath = null;
  }

  public static void setContentType (ContentType type){
    given().contentType(type);
  }

  @Given("^request's content type \"([^\"]*)\" with charset \"([^\"]*)\" is set up$")
  public static void setContentTypeWithCharset (ContentType type, String charset){
    Log.info("Setting up request's Content-Type '" + type + "' with charset '" + charset + "'.");
    requestSpecification = given().contentType(type.withCharset(charset));
  }

  @Given("^request's header \"([^\"]*)\" with value \"([^\"]*)\" is set up$")
  public static void setHeader(String key, String value) {
    Log.info("Setting up request's header '" + key + "' with value '" + value + "'.");
    headersMap.put(key, value);
  }

  public static Map<String, String> getHeadersMap() {
    return headersMap;
  }

  public static RequestSpecification getRequestSpecification() {
    return requestSpecification;
  }

  private static String getMessageAboutWhatRestAssuredMethodIsReset(String item) {
    return "Resetting RestAssured." + item + " to 'null'.";
  }
}
