package stepDefinitions;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import stepDefinitions.api.post.BankAccount;
import utilities.Log;

public class Assertion {
  private final String ERROR_400xx = "400.00";

  @Then("^response's status code (\\d+) is verified$")
  public void checkStatusCode(int expectedStatusCode) {
    Log.info("Asserting that response's status code is " + expectedStatusCode + ".");
    Assert.assertEquals("Received status code - " + BankAccount.getRes().getStatusCode() + getResponseBody(BankAccount.getRes()),
        expectedStatusCode,
        BankAccount.getRes().getStatusCode());
  }

  @Then("^response's 'riskCheckMessage.code' is \"([^\"]*)\"$")
  public void checkRiskCheckMessageCode(String expectedCode) {
    Log.info("Asserting that response's 'riskCheckMessage.code' is '" + expectedCode + "'.");
    String receivedCode = getRiskCheckMessageCode(BankAccount.getRes());
    Assert.assertEquals("Expected code - " + expectedCode + "; Received code - " + receivedCode + getResponseBody(BankAccount.getRes()),
        expectedCode,
        receivedCode);
  }

  @Then("^response verifies \"([^\"]*)\" length of IBAN code$")
  public void checkIBANlength(String sizeOfIBANcode) {
    Log.info("Asserting IBAN code's length.");
    int lengthNumber = BankAccount.getIBANCode().length();

    checkStatusCode(400);
    if (lengthNumber < 7) {
      Log.info("Asserting that IBAN code's length is > 7.");
      Assert.assertEquals("IBAN code's length is >= 7: " + lengthNumber, ERROR_400xx + "6", getBodyErrorCode(BankAccount.getRes()));
    } else if(lengthNumber > 34) {
      Log.info("Asserting that IBAN code's length is < 34.");
      Assert.assertEquals("IBAN code's length is <= 34: " + lengthNumber, ERROR_400xx + "5", getBodyErrorCode(BankAccount.getRes()));
    } else {
      Log.error("IBAN code's length is in acceptable range: 6 < " + lengthNumber + " < 35. Investigate further for 'BusinessError' " +
          "details.");
    }
  }

  private String getResponseBody(Response res) {
    return "\n" + "Body response: " + res.body().asString();
  }

  private String getRiskCheckMessageCode(Response res) {
    return getJsonPath(res).getString("riskCheckMessages.code[0]");
  }

  private String getBodyErrorCode(Response res) {
    return getJsonPath(res).getString("code[0]");
  }

  private String getBodyErrorMessage(Response res) {
    return getJsonPath(res).getString("message[0]");
  }

  private JsonPath getJsonPath (Response res) {
    return new JsonPath(res.asString());
  }
}
