package enums;

import utilities.RandomGenerator;

public enum IBANS {
  validIBAN("DE8937040044" + getRandomAccountNumber(15)),
  invalidIBAN("DE8937007398ED"),
  shortIBAN("CH9300"),
  longIBAN("CH930076201162385295709876543210987"),
  unsupportedCountryIBAN("GB09HAOE91311808002317");

  private String iban;

  IBANS(String iban) {
    this.iban = iban;
  }

  public String getIBAN() {
    return iban;
  }

  private static String getRandomAccountNumber(int maxAccountNumberLength) {
    return RandomGenerator.composeRandomNumberString(maxAccountNumberLength);
  }
}
