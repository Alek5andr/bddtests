package utilities;

import java.util.Random;

public class RandomGenerator {

  public static String composeRandomNumberString(int stringLength) {
    StringBuilder stringBuilder = new StringBuilder();
    int maxDigit = 10;
    int maxStringLength = generateRandomNumber(stringLength);

    for (int i = 0; i < maxStringLength; ++i) {
      Random random = new Random();
      stringBuilder.append(random.nextInt(maxDigit));
    }

    return stringBuilder.toString();
  }

  public static int generateRandomNumber(int endBoundary) {
    if (endBoundary >= 10 && endBoundary <= 20) {
      return (new Random().nextInt(endBoundary) + 1);
    }
    throw new IllegalArgumentException("Requirements not satisfied: endBoundary >= 10 && endBoundary <= 20.");
  }
}
